// Fetch past configurations from local storage and display them in table
let pastConfigurations = localStorage.getItem("pastconfigurations") ?? "[]";
pastConfigurations = JSON.parse(pastConfigurations);
displayPastConfigurations();

function displayPastConfigurations() {
  const confTableBody = document.getElementById("configuration-table");

  let rows = "";
  pastConfigurations.forEach((conf) => {
    rows += `<tr><th scope='row'>${conf.timestamp}</th><td>${conf.component}</td><td>${conf.replicas}</td><td>${conf.public}</td><td>${conf.ip}</td><td>${conf.port}</td></tr>`;
  });

  if (rows.length === 0) {
    rows = "<tr><td colspan='6'>Nothing to display yet.</td></tr>";
  }

  confTableBody.innerHTML = rows;
}
