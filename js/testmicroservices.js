// Fetch past executions from local storage and display them in table
let pastExecutions = localStorage.getItem("pastexecutions") ?? "[]";
console.log(pastExecutions);
pastExecutions = JSON.parse(pastExecutions);
displayPastExecutions();

const sendRequestButton = document
  .getElementById("sendrequest")
  .addEventListener("click", sendRequest);

function displayPastExecutions() {
  const execTableBody = document.getElementById("exec-table");

  let rows = "";
  pastExecutions.forEach((exec) => {
    rows += `<tr><th scope='row'>${exec.no}</th><td>${exec.timestamp}</td><td>${exec.deviations}</td></tr>`;
  });

  if (rows.length === 0) {
    rows = "<tr><td colspan='3'>Nothing to display yet.</td></tr>";
  }

  execTableBody.innerHTML = rows;
}

function sendRequest() {
  const highestExecNo = pastExecutions.length;
  const timeElapsed = Date.now();
  const now = new Date(timeElapsed);

  const newExec = `{"no": "${
    highestExecNo + 1
  }", "timestamp": "${now.toISOString()}", "deviations": "${Math.floor(
    Math.random() * 100
  )}"}`;
  pastExecutions.push(JSON.parse(newExec));
  localStorage.setItem("pastexecutions", JSON.stringify(pastExecutions));
  displayPastExecutions();
}
