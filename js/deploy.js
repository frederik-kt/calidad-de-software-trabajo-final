// Fetch past deployments from local storage and display them in table
let pastDeployments = localStorage.getItem("pastdeployments") ?? "[]";
pastDeployments = JSON.parse(pastDeployments);
displayPastDeployments();

function displayPastDeployments() {
  const depTableBody = document.getElementById("deployment-table");

  let rows = "";
  pastDeployments.forEach((deployment) => {
    rows += `<tr><th scope='row'>${deployment.timestamp}</th><td>${deployment.revision}</td><td>${deployment.component}</td><td>${deployment.successful}</td></tr>`;
  });

  if (rows.length === 0) {
    rows = "<tr><td colspan='4'>Nothing to display yet.</td></tr>";
  }

  depTableBody.innerHTML = rows;
}
