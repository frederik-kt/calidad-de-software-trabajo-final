// Fetch past configurations from local storage and display them in table
let pastConfigurations = localStorage.getItem("pastconfigurations") ?? "[]";
pastConfigurations = JSON.parse(pastConfigurations);
displayPastConfigurations();

const applyConfigButton = document
  .getElementById("applyconfig")
  .addEventListener("click", applyConfig);

function displayPastConfigurations() {
  const confTableBody = document.getElementById("configuration-table");

  let rows = "";
  pastConfigurations.forEach((conf) => {
    rows += `<tr><th scope='row'>${conf.timestamp}</th><td>${conf.component}</td><td>${conf.replicas}</td><td>${conf.public}</td><td>${conf.ip}</td><td>${conf.port}</td></tr>`;
  });

  if (rows.length === 0) {
    rows = "<tr><td colspan='6'>Nothing to display yet.</td></tr>";
  }

  confTableBody.innerHTML = rows;
}

function applyConfig() {
  const timeElapsed = Date.now();
  const now = new Date(timeElapsed);
  const component = document.getElementById("componentinput").value;
  const replicas = document.getElementById("replicainput").value;
  const publicstate = document.getElementById("publicinput").value;
  const ip = document.getElementById("ipinput").value;
  const port = document.getElementById("portinput").value;

  if (port < 30000 || port > 35000) {
    alert("Port must be between 30000 and 35000");
  }

  if (port < 30000 || port > 35000) {
    return;
  }

  const newConf = `{"timestamp": "${now.toISOString()}", "component": "${component}", "replicas": "${replicas}", "public": "${publicstate}", "ip": "${ip}", "port": "${port}"}`;
  pastConfigurations.push(JSON.parse(newConf));
  localStorage.setItem(
    "pastconfigurations",
    JSON.stringify(pastConfigurations)
  );
  displayPastConfigurations();
}
