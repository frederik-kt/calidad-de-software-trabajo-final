// Fetch past executions from local storage and display them in table
let pastRevisions = localStorage.getItem("pastrevisions") ?? "[]";
pastRevisions = JSON.parse(pastRevisions);
displayPastRevisions();

function displayPastRevisions() {
  const revTableBody = document.getElementById("revision-table");

  let rows = "";
  pastRevisions.forEach((exec) => {
    rows += `<tr><th scope='row'>${exec.revision}</th><td>${exec.timestamp}</td><td>${exec.filename}</td><td>${exec.code}</td></tr>`;
  });

  if (rows.length === 0) {
    rows = "<tr><td colspan='4'>Nothing to display yet.</td></tr>";
  }

  revTableBody.innerHTML = rows;
}
