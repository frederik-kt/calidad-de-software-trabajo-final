const clearStorageButton = document
  .getElementById("clearstorage")
  .addEventListener("click", clearStorage);

const downloadJsonButton = document
  .getElementById("downloadjson")
  .addEventListener("click", downloadJson);

const uploadJsonInput = document
  .getElementById("json-input")
  .addEventListener("change", uploadJson);

function clearStorage() {
  localStorage.setItem("pastexecutions", "[]");
  localStorage.setItem("pastrevisions", "[]");
  localStorage.setItem("pastconfigurations", "[]");
  localStorage.setItem("pastdeployments", "[]");
}

function downloadJson() {
  const finalJson = {};
  const pastExecutions = localStorage.getItem("pastexecutions") ?? "[]";
  const pastRevisions = localStorage.getItem("pastrevisions") ?? "[]";
  const pastConfigurations = localStorage.getItem("pastconfigurations") ?? "[]";
  const pastDeployments = localStorage.getItem("pastdeployments") ?? "[]";
  finalJson["pastExecutions"] = pastExecutions;
  finalJson["pastRevisions"] = pastRevisions;
  finalJson["pastConfigurations"] = pastConfigurations;
  finalJson["pastDeployments"] = pastDeployments;

  const dataStr =
    "data:text/json;charset=utf-8," +
    encodeURIComponent(JSON.stringify(finalJson));
  const dlAnchorElem = document.getElementById("downloadAnchorElem");
  dlAnchorElem.setAttribute("href", dataStr);
  dlAnchorElem.setAttribute("download", "trabajo_final.json");
  dlAnchorElem.click();
}

function uploadJson() {
  const files = document.getElementById("json-input").files;
  if (files.length <= 0) {
    return false;
  }

  const fr = new FileReader();

  fr.onload = function (e) {
    const result = JSON.parse(e.target.result);
    localStorage.setItem("pastexecutions", result["pastExecutions"]);
    localStorage.setItem("pastrevisions", result["pastRevisions"]);
    localStorage.setItem("pastconfigurations", result["pastConfigurations"]);
    localStorage.setItem("pastdeployments", result["pastDeployments"]);
  };

  fr.readAsText(files.item(0));
}
