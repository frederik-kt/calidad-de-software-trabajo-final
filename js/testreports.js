// Fetch past executions from local storage and display them in table
let pastExecutions = localStorage.getItem("pastexecutions") ?? "[]";
pastExecutions = JSON.parse(pastExecutions);
displayPastExecutions();

function displayPastExecutions() {
  const execTableBody = document.getElementById("exec-table");

  let rows = "";
  pastExecutions.forEach((exec) => {
    rows += `<tr><th scope='row'>${exec.no}</th><td>${exec.timestamp}</td><td>${exec.deviations}</td></tr>`;
  });

  if (rows.length === 0) {
    rows = "<tr><td colspan='3'>Nothing to display yet.</td></tr>";
  }

  execTableBody.innerHTML = rows;
}
