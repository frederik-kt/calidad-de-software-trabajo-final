# Calidad de Software - Trabajo Final

## Frederik Kutsch

### 03.02.2022

Repository: https://gitlab.com/frederik-kt/calidad-de-software-trabajo-final.git
Deployed at: https://cds-trabajo-final.herokuapp.com/

## Run locally

sudo npm install -g http-server

cd /path/to/calidad_de_software_trabajo_final

http-server
